﻿using PrototypePatternConsoleApp.Model;

namespace PrototypePatternConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ///Для демонстрации самый простой вариант.
            ///Создаю объект. Копирую его через метод Copy интерфейса IMyClonable. Изменяю в копии какое-нибудь свойство
            ///Из копиии создаю еще одну копию, но уже методом Clone интерфейса IClonable
            ///Вывожу все все три объекта в консоль

            var weapon = new Weapon("Какое-то оружие", 0, "Какое-то описание", 0, 0);
            
            var weaponCopy = weapon.Copy();
            weaponCopy.Price = 100500;

            var weaponClone = (Weapon)weaponCopy.Clone();
            /// И вот здесь главное неудобство работы с IClonable - необходимо выполнять присваивание.
            /// В то время как результат работы метода IMyClonable возвращает объект того же типа, какого был исходный.
            weaponClone.Description = $"Выполнено присваивание";

            Console.WriteLine(weapon.ToString());
            Console.WriteLine(weaponCopy.ToString());
            Console.WriteLine(weaponClone.ToString());

            Console.WriteLine("---------------------------------------------");

            var meleeWeapon = new MeleeWeapon("Рукопашное оружие", 100, "Какое-то описание", 1, 8, 5, 1, false);

            var meleeWeaponCopy = meleeWeapon.Copy();
            meleeWeaponCopy.Weight = 10;
            meleeWeaponCopy.Length = 10;

            var meleeWeaponClone = (MeleeWeapon)meleeWeaponCopy.Clone();
            meleeWeaponClone.IsTwoHand = true;
            meleeWeaponClone.Description = $"Двуручное рукопашное оружие";


            Console.WriteLine(meleeWeapon.ToString());
            Console.WriteLine(meleeWeaponCopy.ToString());
            Console.WriteLine(meleeWeaponClone.ToString());

            Console.WriteLine("---------------------------------------------");

            var sword = new Sword("Полуторный меч", 100, "Легендарный меч. Одноручный,но не годится для фехтования", 1, 8, 5, 1.5m, false, false);

            var swordCopy = sword.Copy();
            swordCopy.Name = "Шпага";
            swordCopy.Description = "Одноручный. Подходит для фехтования";
            swordCopy.Weight = 3;
            swordCopy.Length = 1.2m;
            swordCopy.IsFencing = true;

            var swordClone = (Sword)swordCopy.Clone();
            swordClone.Name = "Рубило";
            swordClone.IsTwoHand = true;
            swordClone.Description = $"Двуручное рукопашное оружие";


            Console.WriteLine(sword.ToString());
            Console.WriteLine(swordCopy.ToString());
            Console.WriteLine(swordClone.ToString());

            Console.WriteLine("---------------------------------------------");

            //А здесь ничего изменять не будем. Просто создаем три разных объекта, копируя один из другого

            var hammer = new Hammer("Молот", 100, "Можно гвозди, а можно и по голове", 1, 10, 10, 1, false, 5);

            var hammerCopy = hammer.Copy();

            var hammerClone = (Hammer)hammerCopy.Clone();

            Console.WriteLine(hammer.ToString());
            Console.WriteLine(hammerCopy.ToString());
            Console.WriteLine(hammerClone.ToString());

            if(hammer.Equals(hammerCopy) || hammer.Equals(hammerClone) || hammerCopy.Equals(hammerClone))
            {
                Console.WriteLine("Упс. Какой-то объект есть один и тот же");
            }
            else
            {
                Console.WriteLine("Не смотря на одинаковые значения свойств - это три разных объекта");
            }


            /// Аналогично можно прописать подобные для класса RangedWeapon и его наследника Bow

            Console.ReadKey();
        }
    }
}