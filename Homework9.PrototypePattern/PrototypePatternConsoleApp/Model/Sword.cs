﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePatternConsoleApp.Model
{
    internal class Sword: MeleeWeapon, IMyClonable<Sword>, ICloneable
    {

        public bool IsFencing { get; set; }
        public Sword(string name, decimal price, string description, int minDamage, int maxDamage, decimal weight, decimal length, bool isTwoHand, bool isFencing) :
            base(name, price, description, minDamage, maxDamage, weight, length, isTwoHand)
        {
            IsFencing = isFencing;
        }

        public new Sword Copy()
        {
            return new(Name, Price, Description, MinDamage, MaxDamage, Weight, Length, IsTwoHand, IsFencing);
        }

        public object Clone()
        {
            return Copy();
        }

    }
}
