﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePatternConsoleApp.Model
{
    internal class Hammer : MeleeWeapon, IMyClonable<Hammer>, ICloneable
    {
        public Hammer(string name, decimal price, string description, int minDamage, int maxDamage, decimal weight, decimal length, bool isTwoHand, int stunningTimeSec) : 
            base(name, price, description, minDamage, maxDamage, weight, length, isTwoHand)
        {
            StunningTimeSec = stunningTimeSec;
        }

        public int StunningTimeSec {get; set;}

        public new Hammer Copy()
        {
            return new(Name, Price, Description, MinDamage, MaxDamage, Weight, Length, IsTwoHand, StunningTimeSec);
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
