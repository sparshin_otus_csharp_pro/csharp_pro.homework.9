﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePatternConsoleApp.Model
{
    internal class RangedWeapon: Weapon, IMyClonable<RangedWeapon>, ICloneable
    {
        public decimal Range { get; set; }
        public RangedWeapon(string name, decimal price, string description, int minDamage, int maxDamage, decimal range) :
            base(name, price, description, minDamage, maxDamage)
        {
            Range = range;
        }

        public new RangedWeapon Copy()
        {
            return new(Name, Price, Description, MinDamage, MaxDamage, Range);
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
