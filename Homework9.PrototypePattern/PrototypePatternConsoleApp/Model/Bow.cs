﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePatternConsoleApp.Model
{
    internal class Bow: RangedWeapon, IMyClonable<Bow>, ICloneable
    {
        public decimal ArmorPiercingChance { get; set; }

        public Bow(string name, decimal price, string description, int minDamage, int maxDamage, decimal range, decimal armorPiercingChance) : 
            base(name, price, description, minDamage, maxDamage, range)
        {
            ArmorPiercingChance = armorPiercingChance;
        }

        public new Bow Copy()
        {
            return new(Name, Price, Description, MinDamage, MaxDamage, Range, ArmorPiercingChance);
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
