﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePatternConsoleApp.Model
{
    internal class Weapon : IMyClonable<Weapon>, ICloneable
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public int MinDamage { get; set; }
        public int MaxDamage { get; set; }

        public Weapon(string name, decimal price, string description, int minDamage, int maxDamage)
        {
            Name = name;
            Price = price;
            Description = description;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
        }

        public Weapon Copy()
        {
            return new(Name, Price, Description, MinDamage, MaxDamage);
        }

        public object Clone()
        {
            return Copy();
        }

        public override string ToString()
        {

            ///здесь еще можно было бы позапариваться с тз порядка сортировки 
            ///(сперва базовый класс, потом потомки, но задачка не про это, а выводить одинаковые свойства оно и так будет)
            var properties = this.GetType().GetProperties().Select(t => $"{t.Name}: {t.GetValue(this, null)}").ToList();

            return string.Join("; ", properties);
        }

    }
}
