﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePatternConsoleApp.Model
{
    internal class MeleeWeapon: Weapon, IMyClonable<MeleeWeapon>, ICloneable
    {
        public decimal Weight { get; set; }
        public decimal Length { get; set; }
        public bool IsTwoHand { get; set; }

        public MeleeWeapon(string name, decimal price, string description, int minDamage, int maxDamage, decimal weight, decimal length, bool isTwoHand)
            : base(name, price, description, minDamage, maxDamage)
        {
            Weight = weight;
            Length = length;
            IsTwoHand = isTwoHand;
        }

        public new MeleeWeapon Copy()
        {
            return new (Name, Price, Description, MinDamage, MaxDamage, Weight, Length, IsTwoHand);
        }

        public object Clone()
        {
            return Copy();
        }

    }
}
